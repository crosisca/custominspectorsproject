﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WizardNewCharacter : ScriptableWizard
{
    [Header("Character")]
    public string characterName;
    public Tribe tribe = Tribe.UNSET;
    public Role role = Role.UNSET;

    [Header("Stats")]
    public float MaxHealth = 200;
    public float HealOvertime = 0.5f;
    public float HealPerVitalityLevel = 0.1f;
    public float MovementSpeed = 4;
    public float PowerPerLevel = 10;
    public float VitalityPerLevel = 50;
    public float SpeedPerLevel = 0.03f;

    [Header("Attributes Table")]
    [AttributeTable]
    public float[] power;

    //[Header("Abilities")]
    //public WizardNewCharacterAbility passiveAbility;
    //public WizardNewCharacterAbility basicAttackAbility;
    //public WizardNewCharacterAbility qAbility;
    //public WizardNewCharacterAbility eAbility;
    //public WizardNewCharacterAbility rAbility;
    //public WizardNewCharacterAbility fAbility;

    [MenuItem("KinshipEntertainment/New Character Base")]
    static void CreateWizard ()
    {
        DisplayWizard<WizardNewCharacter>("New Character Base", "Confirm", "Cancel");
    }

    void OnWizardCreate()
    {
        
    }

    void OnWizardOtherButton()
    {
        
    }

    protected override bool DrawWizardGUI()
    {
        return base.DrawWizardGUI();
    }
}

public enum WizardNewCharacterAbilityType
{
    Passive,
    MeleeAttack,
    RangedAttack,
    Cast,
    DeployEntity,
    DeployActor,
    Intervention
}

public enum Tribe
{
    UNSET = 0,
    FORGEBORN = 1,
    TRUNKAR = 2,
    TROOP = 3,
    ARENA = 99
}

public enum Role
{
    UNSET = 0,
    BUILDER = 1,
    DEFENSIVE = 2,
    FIGHTER = 3,
    GENERAL = 4,
    GUARDIAN = 5,
    OFFENSIVE = 6,
    SUPPORT = 7,
    TANK = 8,
    MARKSMAN = 9
}
