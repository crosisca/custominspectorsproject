﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(AttributeTableAttribute))]
public class AttributeTablePropertyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
    {
        return 30;
    }

    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(property.intValue.ToString());
       
        for (int i = 0; i < property.arraySize - 1; i++)
            EditorGUI.PropertyField(position, property.GetArrayElementAtIndex(i).ToString());

        EditorGUILayout.EndHorizontal();
    }
}