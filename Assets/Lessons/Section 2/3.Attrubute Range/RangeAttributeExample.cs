﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttributeExample : MonoBehaviour
{

    [Range(0, 10)]
    public int skillIntValue;

    [Range(0, 10)]
    public float skillFloatValue;
}
