﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace lesson5
{
    public class AttributesExample : MonoBehaviour
    {
        [Header("This is a space for number variables.")]
        [Range(0, 10)]
        public float skillValue;

        [Space(20)]
        [Header("This is a space for literal variables.")]
        [Multiline]
        public string myText;

        [Tooltip("This is a description that shows up when you mouse over the field in inspector")]
        public string obscureVariable;

	    // Use this for initialization
	    void Start () {
		
	    }
	
	    // Update is called once per frame
	    void Update () {
		
	    }
    }
}
