﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultilineAttributeExample : MonoBehaviour
{
    [Multiline]
    public string myText;
}
