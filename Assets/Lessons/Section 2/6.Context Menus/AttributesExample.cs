﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace lesson6
{
    public class AttributesExample : MonoBehaviour
    {
        [Header("This is a space for number variables.")]
        [Range(0, 10)]
        public float skillValue;

        [Space(20)]
        [Header("This is a space for literal variables.")]
        [Multiline]
        public string myText;

        [Tooltip("This is a description that shows up when you mouse over the field in inspector")]
        public string obscureVariable;

        [ContextMenu("Reset To Defaults")]
        void ResetToDefault()
        {
            skillValue = 4;
            myText = "This value has been reseted to default.";
            obscureVariable = "I'm obscuure";
        }
    }
}
