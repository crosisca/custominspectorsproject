﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class InventoryEditorWindow : EditorWindow
{
    string[] toolbarOptions = new string[2] { "Edit", "Create" };
    int currentToolbarOption;
    ItemDatabase itemDatabase;
    Item currentItemBeingEdited;
    Vector2 currentEditViewScrollPos;


    [MenuItem("Window/Inventory Editor")]
    static void Initialize()
    {
        InventoryEditorWindow myWindow = GetWindow<InventoryEditorWindow>();
        myWindow.titleContent = new GUIContent("Inventory");
    }


    void OnGUI()
    {
        if (itemDatabase == null)
            currentItemBeingEdited = null;

        itemDatabase = (ItemDatabase)EditorGUILayout.ObjectField("ItemDatabase Instance", itemDatabase, typeof(ItemDatabase), false);

        if (itemDatabase != null)
        {
            //Rect toolbarPosition = EditorGUILayout.GetControlRect();
            //currentToolbarOption = GUI.Toolbar(new Rect(0, 0, toolbarPosition.width, 50), currentToolbarOption, toolbarOptions);
            currentToolbarOption = GUILayout.Toolbar(currentToolbarOption, toolbarOptions);

            EditorGUILayout.Space();

            if (currentToolbarOption == 0)
                DrawEdit();
            else if (currentToolbarOption == 1)
                DrawCreate();
        }
        else
        {
            EditorGUILayout.HelpBox("Please assign a valid ItemDatabase instance above.", MessageType.Error);
        }
    }

    void DrawEdit()
    {
        Item itemToBeRemoved = null;

        currentEditViewScrollPos = EditorGUILayout.BeginScrollView(currentEditViewScrollPos);
        foreach (Item item in itemDatabase.database)
        {
            DisplayItemEditOptions(item);

            GUI.color = Color.red;

            if (GUILayout.Button("Delete"))
                itemToBeRemoved = item;

            GUI.color = Color.white;

            GUILayout.Space(20);
        }
        EditorGUILayout.EndScrollView();

        if (itemToBeRemoved != null)
            itemDatabase.database.Remove(itemToBeRemoved);
    }

    void DrawCreate()
    {
        //EditorGUILayout.HelpBox("create tab", MessageType.Info);
        if (currentItemBeingEdited != null)
        {
            DisplayItemEditOptions(currentItemBeingEdited);
            if (GUILayout.Button("Create"))
            {
                if (!IsIdTaken(currentItemBeingEdited.id))
                {
                    itemDatabase.database.Add(currentItemBeingEdited);
                    currentItemBeingEdited = null;
                    Debug.Log("This item was added to the database");
                }
                else
                    Debug.LogError("This ID is already taken. Please choose another one.");
            }
        }
        else
            currentItemBeingEdited = new Item(itemDatabase.database.Count, "New Item", "New Description", null, false, null);

    }

    void DisplayItemEditOptions(Item item)
    {
        //Editing
        if(currentToolbarOption == 0)
            EditorGUILayout.IntSlider("ID", item.id, 0, 500);
        //Creating
        else if (currentToolbarOption == 1)
            item.id = EditorGUILayout.IntSlider("ID", item.id, 0, 500);
        

        item.name = EditorGUILayout.TextField("Name", item.name);
        item.description = EditorGUILayout.TextField("Description", item.description);
        item.stackable = EditorGUILayout.Toggle("Stackable", item.stackable);
        item.icon = (Texture2D)EditorGUILayout.ObjectField("Icon", item.icon, typeof(Texture2D), false);
        item.gameObject = (GameObject)EditorGUILayout.ObjectField("Game Object", item.gameObject, typeof(GameObject), false);
    }

    bool IsIdTaken(int id)
    {
        foreach (Item i in itemDatabase.database)
        {
            if (i.id == id)
                return true;
        }

        return false;
    }
}
