﻿using System;
using UnityEngine;

[Serializable]
public class Item
{
    public int id;

    public string name;

    public string description;

    public Texture2D icon;

    public bool stackable;

    public GameObject gameObject;

    public Item (int id, string name, string description, Texture2D icon, bool stackable, GameObject gameObject)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.stackable = stackable;
        this.gameObject = gameObject;
    }
}
