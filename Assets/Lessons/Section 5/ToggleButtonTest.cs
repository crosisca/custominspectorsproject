﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleButtonTest : MonoBehaviour
{
    public bool myBool;

    //[ToggleButtonAttribute]
    [ToggleButton("MyCustomBoolWithAttribute State")]//No need to write Attribute at the end
    public bool myBoolWithAttribute;
}
