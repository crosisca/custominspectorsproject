﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ToggleButtonAttribute))]
public class ToggleButtonAttributePropertyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 30;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        ToggleButtonAttribute attribute = base.attribute as ToggleButtonAttribute;
        bool value = property.boolValue;
        Color buttonColor = Color.green;
        //string description = property.displayName + " - On";
        string description = attribute.customDescription + " - On";

        if (!value)
        {
            buttonColor = Color.red;

            description = attribute.customDescription + " - Off";
            //description = property.displayName + " - Off";
        }

        position.height = 30;

        GUI.color = buttonColor;
        if (GUI.Button(position, description))
            property.boolValue = !property.boolValue;
        GUI.color = Color.white;
    }
}
