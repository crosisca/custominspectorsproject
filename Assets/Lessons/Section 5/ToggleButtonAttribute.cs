﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleButtonAttribute : PropertyAttribute
{
    public string customDescription;

    public ToggleButtonAttribute (string customDescription)
    {
        this.customDescription = customDescription;
    }
}
