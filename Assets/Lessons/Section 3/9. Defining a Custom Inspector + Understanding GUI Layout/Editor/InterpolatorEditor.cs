﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace lesson9
{
    [CustomEditor(typeof(Interpolator))]
    public class InterpolatorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.HelpBox("There's nothing here.", MessageType.Warning);
            EditorGUILayout.HelpBox("Actually, there's something here!", MessageType.Info);
            EditorGUILayout.EndHorizontal();

            bool hasTheButtonBeenClicked = GUILayout.Button("Click me");

            if (hasTheButtonBeenClicked)
                Debug.Log("The button was clicked");



        }
    }
}
