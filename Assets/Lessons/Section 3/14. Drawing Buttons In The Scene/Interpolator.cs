﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace lesson14
{
    public class Interpolator : MonoBehaviour
    {

        public Vector3 from;
        public Vector3 to;

        public float interpolatorValue;

        public bool drawLine;
        public bool drawHandlers;
        public bool drawPositionButtons;

        public Vector3 Interpolate(float value)
        {
            Vector3 diffVector = to - from;
            Vector3 finalPos = from + diffVector * value;
            return finalPos;
        }
    }
}
