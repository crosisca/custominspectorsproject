﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace lesson10
{
    [CustomEditor(typeof(Interpolator))]
    public class InterpolatorEditor : Editor
    {
        int currentlySelected;
        new Interpolator target;

        void OnEnable()
        {
            target =base.target as Interpolator;
        }

        void OnDisable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            string[] options = new string[2] {"Edit", "Simuulate"};

            currentlySelected = GUILayout.Toolbar(currentlySelected, options);

            if(currentlySelected == 0)
                DrawEditInspector();
            else if(currentlySelected == 1)
                DrawSimuulateInspector();

        }

        void DrawEditInspector()
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Define as 'origin'"))
                target.from = target.transform.position;

            if (GUILayout.Button("Define as 'destination'"))
                target.to = target.transform.position;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Move to origin"))
                target.transform.position = target.from;

            if (GUILayout.Button("Move to destination"))
                target.transform.position = target.to;
            EditorGUILayout.EndHorizontal();
        }

        void DrawSimuulateInspector ()
        {

        }
    }
}
