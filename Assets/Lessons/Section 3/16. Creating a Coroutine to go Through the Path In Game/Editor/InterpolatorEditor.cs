﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace lesson16
{
    [CustomEditor(typeof(Interpolator))]
    public class InterpolatorEditor : Editor
    {
        int currentlySelected;
        new Interpolator target;

        void OnEnable()
        {
            target =base.target as Interpolator;
        }

        void OnDisable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            string[] options = new string[2] {"Edit", "Simulate"};

            currentlySelected = GUILayout.Toolbar(currentlySelected, options);
            
            EditorGUILayout.Space();
            //GUILayout.Space();//Requires space in pixels
            
            if(currentlySelected == 0)
                DrawEditInspector();
            else if(currentlySelected == 1)
                DrawSimulateInspector();

            
        }
        
        public void OnSceneGUI()
        {
            if (target.drawLine)
            {
                Handles.color = Color.yellow;
                Handles.DrawLine(target.from, target.to);

                Handles.SphereHandleCap(0, target.from, Quaternion.identity, HandleUtility.GetHandleSize(target.from) * 0.1f, EventType.Repaint); //constant size
                Handles.SphereHandleCap(0, target.to, Quaternion.identity, 0.5f, EventType.Repaint); //size differs with camera distance
                Handles.color = Color.white;
            }

            //Doesnt work..currentlySelected is always 0
            //if (currentlySelected == 0)
            if(target.drawHandlers)
            {
                target.from = Handles.DoPositionHandle(target.from, Quaternion.identity);
                target.to = Handles.DoPositionHandle(target.to, Quaternion.identity);
            }

            if (target.drawPositionButtons)
            {
                Handles.BeginGUI();
                foreach (Transform transform in FindObjectsOfType<Transform>())
                {
                    if (transform != target.transform && transform.position != target.from && transform.position != target.to)
                    {
                        Vector2 guiPosition = HandleUtility.WorldToGUIPoint(transform.position);
                        Rect currentRect = new Rect(guiPosition, new Vector2(80, 20));
                        bool isNewOrigin = GUI.Button(currentRect, "Origin");
                        if (isNewOrigin)
                            target.from = transform.position;

                        currentRect.position += new Vector2(0, 21);
                        bool isNewDestination = GUI.Button(currentRect, "Destination");
                        if(isNewDestination)
                            target.to = transform.position;
                    }
                }
                Handles.EndGUI();
            }
        }

        void DrawEditInspector()
        {
            GUI.color = Color.grey * 0.3f;//multiplying by 0.x also multiplies alpha channel, making the box lighter
            Rect currentDrawingRectangle = EditorGUILayout.GetControlRect(false, 1);

            float padding = 3;

            Rect resizedRect = new Rect(currentDrawingRectangle.x - padding, currentDrawingRectangle.y,
                currentDrawingRectangle.width + padding * 2, 40 + padding * 2);

            GUI.Box(resizedRect, "");
            GUI.color = Color.white;

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Define as 'origin'"))
                target.from = target.transform.position;

            if (GUILayout.Button("Define as 'destination'"))
                target.to = target.transform.position;
            EditorGUILayout.EndHorizontal();

            GUI.color = Color.yellow;
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Move to origin"))
                target.transform.position = target.from;

            if (GUILayout.Button("Move to destination"))
                target.transform.position = target.to;
            EditorGUILayout.EndHorizontal();
            GUI.color = Color.white;

            EditorGUILayout.BeginHorizontal();
            target.drawLine = DrawToggleButton(target.drawLine, "Debug Line");
            target.drawHandlers = DrawToggleButton(target.drawHandlers, "Debug Handlers");
            target.drawPositionButtons = DrawToggleButton(target.drawPositionButtons, "Debug Position Buttons");
            EditorGUILayout.EndHorizontal();
        }
        
        void DrawSimulateInspector ()
        {
            //This makes sure I'm only checking for changes on the slider on the next (Gui.changed)
            GUI.changed = false;

            target.interpolatorValue = EditorGUILayout.Slider("Interpolation", target.interpolatorValue, 0, 1);

            if (GUI.changed)
            {
                Vector3 position = target.Interpolate(target.interpolatorValue);
                target.transform.position = position;
            }
        }

        bool DrawToggleButton(bool status, string label)
        {
            string text = string.Empty;
            Color color = Color.white;

            if (status)
            {
                text = label + " - On";
                color = Color.green;
            }
            else
            {
                text = label + " - Off";
                color = Color.red;
            }

            GUI.color = color;
            if (GUILayout.Button(text))
                return !status;
            else
                return status;
        }
    }
}

#endif