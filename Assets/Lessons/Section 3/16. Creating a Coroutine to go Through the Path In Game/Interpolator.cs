﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace lesson16
{
    public class Interpolator : MonoBehaviour
    {

        public Vector3 from;
        public Vector3 to;

        public float interpolatorValue;

        public bool drawLine;
        public bool drawHandlers;
        public bool drawPositionButtons;

        void Start()
        {
            StartCoroutine(InterpolationCoroutine(2));
        }

        public Vector3 Interpolate(float value)
        {
            Vector3 diffVector = to - from;
            Vector3 finalPos = from + diffVector * value;
            return finalPos;
        }

        IEnumerator InterpolationCoroutine(float duration)
        {
            interpolatorValue = 0;
            while (interpolatorValue < 1)
            {
                interpolatorValue += Time.deltaTime / duration;
                transform.position = Interpolate(interpolatorValue);
                yield return null;
            }
            interpolatorValue = 1;
        }
    }
}
